if [ $1 = "-loop" ]
then 
	while :
	do
        mpost -interaction=batchmode -s 'outputformat="svg"' --mem=metafun mp/dessin.mp
        mpost -interaction=batchmode -s 'outputformat="svg"' dessin.mp
        mv dessin.[[:digit:]] svg/dessin.svg
        context metafun.tex
        inkscape \
        --without-gui \
        --file=metafun.pdf \
        --export-plain-svg=output.svg 
        mv output.svg svg/output.svg
        #svg/$LETTRE.svg
        #rm *.log 
	sleep 3
	done
else
        mpost -interaction=batchmode -s 'outputformat="svg"'--mem=metafun mp/dessin.mp
        mv dessin.[[:digit:]] svg/dessin.svg    
fi

#mpost '@metafun' myfigure.mp
