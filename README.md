# meta-bac-a-sable

## Organisation des fichiers

Le fichier `dessin.mp` permet de dessiner en Metapost. Bien vérifier qu'il commence par `input ../def; beginfig();` et se termine par `endfig();`

Le fichier `def.mp` contient les fonctions communes à tous les fichiers Metapost

Le fichier `metafun.tex` permet de dessiner en Metapost avec ConTexT en utilisant l'extension Metafun (plus de fonctionnalités + permet d'écrire du texte et le modifier)
(Pour utiliser Metafun, télécharger ConTeXt --> vérifier si ça fonctionne sans)  
Pour commencer à dessiner avec Metafun, écrire le code Metapost entre `\startMPpage` et `\stopMPpage`
Doc Metafun : https://wiki.contextgarden.net/MetaFun_-_MetaPost_in_ConTeXt http://www.pragma-ade.com/general/manuals/metafun-p.pdf


## Générer les SVG

Dans le terminal : `bash generate.sh <nom du fichier mp>` ou `bash generate.sh -loop` pour générer automatiquement les svg toutes les 3s

Par exemple, pour générer le svg de `dessin.mp` : `bash generate.sh dessin`

Le fichier `metafun.tex` s'exporte en pdf et se convertit automatiquement en svg lorsque l'on execute la commande


## Afficher les svg

Lancer index.php sur localhost : il récupère tous les svg du dossier `svg` et les affiche
